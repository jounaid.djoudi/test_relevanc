

def read(file):
    '''lecture du fichier'''
    while True:
        line = file.readline()
        if line is None:
            break
        yield line

def split_delimiter(rows, delimiter):
    '''split chaque ligne par rapport au délimiteur donné en paramètre'''
    for row in rows:
        yield row.split(delimiter)

def select_code_magasin_prix(rows):
    '''selection du code magasin et du prix pour chaque ticket de caisse'''
    for row in rows:
        yield row[3],row[5].replace("\n","")

def keep_rows_with_only_digits(rows):
    '''garde uniquement les lignes avec des digits'''
    for row in rows:
        for i in range(len(row)-1):
            if row[i].isdigit():
                yield row[0],row[1]

with open("randomized-transactions-202009.psv", "r") as file:
    lines = read(file)
    lines = split_delimiter(lines, "|")
    lines_code_mag_prix = select_code_magasin_prix(lines)
    lines_code_mag_prix = keep_rows_with_only_digits(lines_code_mag_prix)
    lines_code_mag_prix = ((x[0], float(x[1])) for x in lines_code_mag_prix) # convertit en float chaque prix
 
    dict_magasin_prix = { k:v for k,v in lines_code_mag_prix} # ne fonctionne pas
    list_magasin_ca = [(key,sum( v for k,v in lines_code_mag_prix if k == key )) for key in dict_magasin_prix.keys()]
    # print(list_magasin_ca)
